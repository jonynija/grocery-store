import 'dart:developer';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:grocery_shop/core/secure_storage/secure_storage_type.dart';

class SecureStorage {
  final storage = const FlutterSecureStorage();

  Future<void> writeValue(String key, dynamic value) async {
    await storage.write(key: key, value: value.toString());
  }

  Future<dynamic> readValue(String key, SecureStorageType type) async {
    try {
      final value = await storage.read(key: key);

      if (value == null) return null;

      switch (type) {
        case SecureStorageType.string:
          return value;
        case SecureStorageType.boolean:
          return (value == 'true');
        case SecureStorageType.number:
          return num.tryParse(value);
      }
    } catch (e) {
      log(e.toString());
    }

    return null;
  }
}
