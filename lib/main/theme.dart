import 'package:flutter/material.dart';

class GroceryTheme {
  static ThemeData theme() {
    return ThemeData(
      primarySwatch: Colors.blue,
      scaffoldBackgroundColor: Colors.white,
    );
  }
}
