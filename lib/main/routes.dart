import 'package:flutter/material.dart';
import 'package:grocery_shop/features/auth/login/screen/login_screen.dart';
import 'package:grocery_shop/features/onboarding/screen/onboarding_screen.dart';
import 'package:grocery_shop/main/route_paths.dart';

class GroceryRoute {
  static MaterialPageRoute routes(RouteSettings settings) {
    // final args = settings.arguments;
    final name = settings.name;

    Widget page;

    switch (name) {
      case RoutePaths.onboarding:
        page = const OnboardingScreen();
        break;
      case RoutePaths.login:
        page = const LoginScreen();
        break;
      default:
        page = const Scaffold();
    }

    return MaterialPageRoute(builder: (_) => page);
  }
}
