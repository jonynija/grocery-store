import 'package:flutter/material.dart';

import 'package:grocery_shop/main/route_paths.dart';
import 'package:grocery_shop/main/routes.dart';
import 'package:grocery_shop/main/theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Grocery Store',
      theme: GroceryTheme.theme(),
      initialRoute: RoutePaths.onboarding,
      onGenerateRoute: GroceryRoute.routes,
    );
  }
}
