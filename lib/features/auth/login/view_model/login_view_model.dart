import 'dart:developer';

import 'package:flutter/widgets.dart'
    show ChangeNotifier, TextEditingController;
import 'package:grocery_shop/services/firebase_auth_service.dart';

class LoginViewModel extends ChangeNotifier {
  final _authService = FirebaseAuthService();

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  void login() async {
    final email = emailController.text.trim();
    final password = passwordController.text.trim();

    final result = await _authService.login(email, password);
    log(result.toString());
  }
}
