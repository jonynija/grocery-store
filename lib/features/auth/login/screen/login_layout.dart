import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

import '../view_model/login_view_model.dart';

class LoginLayout extends StatelessWidget {
  const LoginLayout({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                controller: context.read<LoginViewModel>().emailController,
                decoration: const InputDecoration(
                  hintText: 'Correo',
                ),
                keyboardType: TextInputType.emailAddress,
              ),
              const SizedBox(height: 10),
              TextFormField(
                controller: context.read<LoginViewModel>().passwordController,
                decoration: const InputDecoration(
                  hintText: 'Contraseña',
                ),
                obscureText: true,
                keyboardType: TextInputType.visiblePassword,
              ),
              const SizedBox(height: 25),
              ElevatedButton(
                onPressed: () {
                  context.read<LoginViewModel>().login();
                },
                child: const Text(
                  'Iniciar sesión',
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
