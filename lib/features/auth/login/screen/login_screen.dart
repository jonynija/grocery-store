import 'package:flutter/material.dart';
import 'package:grocery_shop/features/auth/login/screen/login_layout.dart';
import 'package:grocery_shop/features/auth/login/view_model/login_view_model.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => LoginViewModel(),
      child: const Scaffold(
        body: LoginLayout(),
      ),
    );
  }
}
