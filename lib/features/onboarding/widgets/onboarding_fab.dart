import 'package:flutter/material.dart';
import 'package:grocery_shop/features/onboarding/view_model/onboarding_view_model.dart';
import 'package:provider/provider.dart';

class OnboardingFab extends StatelessWidget {
  const OnboardingFab({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        context.read<OnboardingViewModel>().navigateLogin(context);
      },
      child: Icon(Icons.adaptive.arrow_forward),
    );
  }
}
