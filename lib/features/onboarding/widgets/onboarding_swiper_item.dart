import 'package:flutter/material.dart';

class OnboardingSwiperItem extends StatelessWidget {
  const OnboardingSwiperItem({
    super.key,
    required this.title,
    required this.subtitle,
    required this.imagePath,
  });

  final String title;
  final String subtitle;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
            image: AssetImage(imagePath),
          ),
          const SizedBox(height: 15),
          Text(
            title,
            style: Theme.of(context).textTheme.headline3,
          ),
          const SizedBox(height: 15),
          Text(
            subtitle,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          const SizedBox(height: 15),
        ],
      ),
    );
  }
}
