import 'package:flutter/material.dart';
import 'package:grocery_shop/features/onboarding/screen/onboarding_layout.dart';
import 'package:grocery_shop/features/onboarding/view_model/onboarding_view_model.dart';
import 'package:grocery_shop/features/onboarding/widgets/onboarding_fab.dart';
import 'package:provider/provider.dart';

class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => OnboardingViewModel(),
      child: const Scaffold(
        body: OnboardingLayout(),
        floatingActionButton: OnboardingFab(),
      ),
    );
  }
}
