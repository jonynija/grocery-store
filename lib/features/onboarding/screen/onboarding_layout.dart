import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:grocery_shop/features/onboarding/widgets/onboarding_swiper_item.dart';
import 'package:provider/provider.dart';

import '../view_model/onboarding_view_model.dart';

class OnboardingLayout extends StatefulWidget {
  const OnboardingLayout({super.key});

  @override
  State<OnboardingLayout> createState() => _OnboardingLayoutState();
}

class _OnboardingLayoutState extends State<OnboardingLayout> {
  @override
  void initState() {
    super.initState();

    Future.delayed(
      Duration.zero,
      () {
        context.read<OnboardingViewModel>().init(context);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> pages = [
      const OnboardingSwiperItem(
        title: '1 Manage Tasks',
        subtitle:
            'Collaborate, create, and keep track of your project easily and effectively.',
        imagePath: 'assets/onboarding/onboarding_logo.png',
      ),
      const OnboardingSwiperItem(
        title: '2 Manage Tasks Easily',
        subtitle:
            'Collaborate, create, and keep track of your project easily and effectively.',
        imagePath: 'assets/onboarding/onboarding_logo.png',
      ),
      const OnboardingSwiperItem(
        title: '3 Manage Tasks Easily & Effectively.',
        subtitle:
            'Collaborate, create, and keep track of your project easily and effectively.',
        imagePath: 'assets/onboarding/onboarding_logo.png',
      )
    ];

    return SafeArea(
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return pages[index];
        },
        itemCount: pages.length,
        loop: false,
      ),
    );
  }
}
