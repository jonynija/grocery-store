import 'package:flutter/widgets.dart'
    show BuildContext, ChangeNotifier, Navigator;
import 'package:grocery_shop/core/secure_storage/secure_storage.dart';
import 'package:grocery_shop/core/secure_storage/secure_storage_keys.dart';
import 'package:grocery_shop/core/secure_storage/secure_storage_type.dart';
import 'package:grocery_shop/main/route_paths.dart';

class OnboardingViewModel extends ChangeNotifier {
  final secureStorage = SecureStorage();

  void init(BuildContext context) async {
    final value = await secureStorage.readValue(
      SecureStorageKeys.onboardingShowed,
      SecureStorageType.boolean,
    ) as bool?;
    if (value == true) {
      Navigator.pushReplacementNamed(context, RoutePaths.login);
      return;
    }

    secureStorage.writeValue(SecureStorageKeys.onboardingShowed, true);
  }

  void navigateLogin(BuildContext context) {
    Navigator.pushReplacementNamed(context, RoutePaths.login);
  }
}
